package com.kshrd.springsecuritydemopractice.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Accessors(chain = true)
@Data
public class AuthUser {

    private  int id;
    private String username;
    private String password;
    private List<String> roles;
    // for the convinient
    public AuthUser(String username, String password){
        this.username = username;
        this.password = password;


    }
}
