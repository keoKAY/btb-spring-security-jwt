package com.kshrd.springsecuritydemopractice.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Student {


    private  int id;
    private String username;
    private String gender;
    private String bio;
    private  String password;
}
