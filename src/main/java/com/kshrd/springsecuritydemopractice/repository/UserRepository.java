package com.kshrd.springsecuritydemopractice.repository;


import com.kshrd.springsecuritydemopractice.model.AuthUser;
import org.apache.ibatis.annotations.*;
import org.springframework.security.core.userdetails.User;

import java.util.List;

@Mapper
public interface UserRepository {


    @Select("select * from students where username=#{username}")
    @Results({
            @Result(property = "roles",column = "id",many = @Many(select = "findRolesByUserId"))
    })
    AuthUser findUserByUsername(String username);
    @Select("select role_name from roles inner join student_role sr on roles.id = sr.role_id where user_id =#{id}")
    List<String> findRolesByUserId(int id);


}
