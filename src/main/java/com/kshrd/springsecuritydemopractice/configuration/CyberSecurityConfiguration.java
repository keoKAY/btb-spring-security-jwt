package com.kshrd.springsecuritydemopractice.configuration;


import com.kshrd.springsecuritydemopractice.security.UserDetailService;
import com.kshrd.springsecuritydemopractice.security.handler.CustomAuthenticationEntryPoint;
import com.kshrd.springsecuritydemopractice.security.handler.CustomAuthenticationSuccessHandler;
import com.kshrd.springsecuritydemopractice.security.handler.CustomLogoutSuccessHandler;
import com.kshrd.springsecuritydemopractice.security.jwt.EntryPointJwt;
import com.kshrd.springsecuritydemopractice.security.jwt.JwtTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class CyberSecurityConfiguration extends WebSecurityConfigurerAdapter {



    @Autowired
    UserDetailService userDetailService;

//    @Autowired
//    JwtTokenFilter jwtTokenFilter;

    @Autowired
    public EntryPointJwt unauthorizedHandler;
//    @Autowired
//    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;
//    @Autowired
//    private CustomAuthenticationEntryPoint entryPointJwt;

    // used to define which URL paths should be protected or not.
    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        super.configure(http);

//        http.csrf().disable()
//                .authorizeRequests()
//                .antMatchers("/uppercase","/login").permitAll()
//                .anyRequest()
//                .fullyAuthenticated()
//                .and()
//                .formLogin()
//                .loginProcessingUrl("/login")
//                .failureUrl("/login?error=true")
//                .loginPage("/login")
//                .successHandler(customAuthenticationSuccessHandler)
//                .usernameParameter("username")
//                .passwordParameter("password")
//                .and()
//                .logout()
//                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//                .logoutSuccessHandler(new CustomLogoutSuccessHandler())
//                .logoutSuccessUrl("/")
//                .and()
//                .exceptionHandling()
//                .authenticationEntryPoint(entryPointJwt)
//                ;


        http.csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .antMatcher("/api/**")  // any other request to the api must be authorize besides these
                .authorizeRequests()
                .antMatchers("/file/**","/uppercase").permitAll().anyRequest().authenticated();




// For the Jwt Sake
     http.addFilterBefore(jwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }
    @Bean
    public JwtTokenFilter jwtTokenFilter(){
        return new JwtTokenFilter();
    }
    @Bean
    public AuthenticationManager authenticationManager() throws Exception{
        return super.authenticationManager();
    }

//    Works on the authentication process of identifying users
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        super.configure(auth);
        // Using the inmemory authentication

   /*     auth.inMemoryAuthentication().withUser("admin user").password(passwordEncoder().encode("12345")).roles("ADMIN").authorities("EDIT");
        auth.inMemoryAuthentication().withUser("normal user").password(passwordEncoder().encode("12345")).roles("USER");
        auth.inMemoryAuthentication().withUser("handsome user").password(passwordEncoder().encode("12345")).roles("HANDSOME");
*/
//        This is for working with the database.

        auth.userDetailsService(userDetailService)
                .passwordEncoder(passwordEncoder());

    }


    @Bean
    PasswordEncoder passwordEncoder(){
//    return   NoOpPasswordEncoder.getInstance();
       return new BCryptPasswordEncoder();
    }
    // this is used to ignore resources, set debug, reject request....
    @Override
    public void configure(WebSecurity web) throws Exception {
//        super.configure(web);

        web.ignoring()
                .antMatchers("/h2-console/**",
                        "/swagger-ui.html", "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**",
                        "/resources/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**",
                        "/images/**", "/scss/**", "/vendor/**", "/favicon.ico", "/auth/**", "/favicon.png",
                        "/v2/api-docs", "/configuration/ui", "/configuration/security",
                        "/webjars/**", "/swagger-resources/**", "/actuator", "/swagger-ui/**",
                        "/actuator/**", "/swagger-ui/index.html", "/swagger-ui/");
    }



}
