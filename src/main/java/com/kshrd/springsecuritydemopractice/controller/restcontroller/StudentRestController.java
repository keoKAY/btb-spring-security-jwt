package com.kshrd.springsecuritydemopractice.controller.restcontroller;


import com.kshrd.springsecuritydemopractice.model.Student;
import com.kshrd.springsecuritydemopractice.security.UserDetailImp;
import com.kshrd.springsecuritydemopractice.security.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentRestController {


     @Autowired
   AuthenticationManager authenticationManager;
    @GetMapping
    public String test(){

        return " I am return as the string ";
    }

    @PostMapping("/login")
    public String login(@RequestParam String username,@RequestParam String password){

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                = new UsernamePasswordAuthenticationToken(
                        username,password
        );

        try{

            Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);

            JwtUtils jwtUtils  = new JwtUtils();
            String jwtToken = jwtUtils.generateJwtToken(authentication);


            

            return jwtToken;

        }catch (Exception ex){
            System.out.println("Error When login Exception Occurs: ");
            System.out.println(ex.getMessage());
            return null;
        }


    }

    @GetMapping("/uppercase")
    public String testVersion2 (){

        return "I AM UPPERCASE ";
    }


@GetMapping("/forhandsome")
    public String forhandsome(@AuthenticationPrincipal UserDetailImp user){

    System.out.println("Here is the value of the login user : "+user);
        return "For handsome man like me.";
}


    @GetMapping("/foruser")
    public String testVersionUser (){

        return "This is for the user";
    }

}
