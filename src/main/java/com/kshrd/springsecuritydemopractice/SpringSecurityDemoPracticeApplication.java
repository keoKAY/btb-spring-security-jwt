package com.kshrd.springsecuritydemopractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityDemoPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityDemoPracticeApplication.class, args);
    }

}
