package com.kshrd.springsecuritydemopractice.security;

import com.kshrd.springsecuritydemopractice.model.AuthUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDetailImp implements UserDetails {

   private  int id;
   private String username;
   private String password ;
   private Collection<? extends GrantedAuthority> authorities;

   // for the sake of the getting the role of the user

    public static UserDetailImp build(AuthUser user){


        List<GrantedAuthority> authorities = user.getRoles()
                .stream()
                .map(e-> new SimpleGrantedAuthority(e))
                .collect(Collectors.toList());


        return new UserDetailImp(
                user.getId(),
                user.getUsername(),
                user.getPassword()
                ,authorities
        );
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true ;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true ;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
