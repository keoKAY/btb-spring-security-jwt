package com.kshrd.springsecuritydemopractice.security.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {


        // Send the direct after it is successfully authenticated
        System.out.println("onAuthenticationSuccess: here is the value of the authorities");
        authentication.getAuthorities().stream().forEach(System.out::println);



        for (GrantedAuthority auth:authentication.getAuthorities()){

            if (auth.getAuthority().equalsIgnoreCase("admin")){

                System.out.println(" >>>>> Here is the authorities : "+auth.getAuthority());
                response.sendRedirect("/");
                return ;

            }
        }


        // when they try to access anything that is unauthorized we will stored that value into sesssion
        String attemptedURI = (String) request.getSession().getAttribute("ATTEMPTED_URI");
        if (StringUtils.hasText(attemptedURI)){
            response.sendRedirect(attemptedURI);
            return;
        }
        // else
        response.sendRedirect("/login");


    }
}
